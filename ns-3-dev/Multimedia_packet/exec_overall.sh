# 1. TCP congestion window
for queue_type in "CoDel" "Cobalt" "FqCoDel" "PfifoFast" "Pie" "Red"
do
    tcptrace -G "../socket-${queue_type}QueueDisc-3-2.pcap"
    xpl2gpl a2b_owin.xpl
    sed -i '' '/title/d' ./a2b_owin.gpl
    sed -i '' '/labels/d' ./a2b_owin.gpl
    # sed -i '' '/datasets/d' ./a2b_owin.gpl
    # sed -i "6iexec('plot \"a2b_owin.datasets\" index 2 using ($1-946684800.0):2 with lines;');" ./a2b_owin.gpl
    gnuplot a2b_owin.gpl
    mv a2b_owin.ps "cwnd_${queue_type}.ps"

done

# 2. TCP RTT graph and RTT_avg 
# // Check the FQ Codel and the PFIFO
echo "" > "./rtt_avg.txt"
for queue_type in "CoDel" "Cobalt" "FqCoDel" "PfifoFast" "Pie" "Red"
do
    tcptrace -R "../socket-${queue_type}QueueDisc-3-2.pcap"
    echo "${queue_type}" >> "./rtt_avg.txt"
    echo $(tcptrace -lur ../socket-${queue_type}QueueDisc-3-2.pcap | grep "RTT avg:") >> "./rtt_avg.txt"
    xpl2gpl a2b_rtt.xpl
    sed -i '' '/samples/d' ./a2b_rtt.gpl
    sed -i '' '/labels/d' ./a2b_rtt.gpl
    gnuplot a2b_rtt.gpl
    mv a2b_rtt.ps "rtt_${queue_type}.ps"
done

# 3. Queue size graph with a 1ms sampling interval
# gnuplot -e "args='FIFO'" queue_length_indiv.gp
gnuplot -e "args='CODEL'" queue_length_indiv.gp
gnuplot -e "args='COBALT'" queue_length_indiv.gp
gnuplot -e "args='FQCODEL'" queue_length_indiv.gp
gnuplot -e "args='PFIFO'" queue_length_indiv.gp
gnuplot -e "args='PIE'" queue_length_indiv.gp
gnuplot -e "args='RED'" queue_length_indiv.gp

# 4. UDP reception rate (done)

# 5. UDP losses: at Packet level, and frame level

# 6. TCP throughput graph
echo "" > "./throughput_avg.txt"
for queue_type in "CoDel" "Cobalt" "FqCoDel" "PfifoFast" "Pie" "Red"
do
    echo "${queue_type}" >> "./throughput_avg.txt"
    echo $(tcptrace -lu ../socket-${queue_type}QueueDisc-3-2.pcap | grep "throughput") >> "./throughput_avg.txt"
done