class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        if '*' not in s and '*' not in p:
            if len(s) != len(p): return False
            for idx, ele in enumerate(s):
                if ele == '.' or p[idx] == '.' or ele == p[idx]: pass
                else: return False
            return True
        else:
            ssub_str = ''
            psub_str = ''
            for leng in range(max(len(s), len(p))):
                if s[leng] != '*': 
                    if ssub_str[-1] == '*': ssub_str = ssub_str[:-1]
                    ssub_str += s[leng]
                if p[leng] != '*': 
                    if psub_str[-1] == '*': psub_str = psub_str[:-1]
                    psub_str += p[leng]

                if s[leng] == '*':
                    if p[leng] != ssub_str[-1]: return False
                    ssub_str += '*'

                if p[leng] == '*':
                    if s[leng] != psub_str[-1]: return False
                    psub_str += '*'